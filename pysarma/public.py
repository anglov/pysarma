from . import _common


def user_data(id_):
    data = {'userId': id_}
    return _common.request('commonUserData', data)


def user_post_in_province(province_id):
    data = {'prowincja': province_id}
    return _common.request('userPostInProvince', data)


def list_of_province():
    data = {}
    return _common.request('listOfProvince', data)


def list_of_cities():
    data = {}
    return _common.request('listOfCities', data)


def list_of_institutions():
    data = {}
    return _common.request('listOfInstituctions', data)


def edit_forum_group(group_id, group_members, key):
    data = {'groupId': group_id,
            'secureKey': key,
            'groupMembers[]': group_members}
    return _common.request('editForumGroup', data)
