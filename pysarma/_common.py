import requests
import json

BASE_URL = 'http://www.sarmacja.org/integracja/'


def request(function_, data):
    url = BASE_URL + '+' + function_ + '/'
    resp = requests.post(url, data)
    return json.loads(resp.text)
