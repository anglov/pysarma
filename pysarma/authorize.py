from CryptoPlus.Cipher import python_Rijndael
from urllib.parse import urlencode
from urllib.request import pathname2url, url2pathname
import base64
import json
import random
from . import _common


class Integration:
    def __init__(self, app_id, app_secret, options):
        self.app_id = app_id
        self.app_secret = app_secret
        self.options = base64.b64encode(json.dumps(options).encode())
        self.crypt = python_Rijndael.new(self.app_secret.encode(), python_Rijndael.MODE_ECB, blocksize=32)

    def get_url(self, app_name, login_handler):
        authorize_params = {'options': self.options,
                            'redirect': base64.b64encode(login_handler.encode()),
                            'appName': base64.b64encode(app_name.encode()),
                            'appId': self.app_id}
        decoded_params = urlencode(authorize_params)
        authorize_url = _common.BASE_URL + 'auth2?' + decoded_params
        return authorize_url

    def login(self, id_, at):
        payload = {
            'aT': at,
            'upr': self.options.decode(),
            'appS': self.app_secret,
            'userId': id_,
            'appI': self.app_id
        }
        return self._action(payload, 'login')

    def notify(self, id_, text):
        payload = {
            'odbiorca': id_,
            'powiadomienie': text,
            'appS': self.app_secret
        }
        return self._action(payload, 'powiadomienie')

    def transfer_to_user(self, id_, value, title):
        payload = {
            'odbiorca': id_,
            'kwota': value,
            'tytul': title,
            'appS': self.app_secret
        }
        return self._action(payload, 'przelewOdInstytucji')

    def transfer_from_user(self, id_, value, title):
        transaction_id = random.randint(1000, 9999)
        payload = {
            'przelewId': transaction_id,
            'nadawca': id_,
            'kwota': value,
            'tytul': title,
            'appS': self.app_secret,
        }
        res = self._action(payload, 'przelew')
        if res['error'] != 200:
            return res
        unb64 = base64.b64decode(url2pathname(res['body']))
        decrypted = self.crypt.decrypt(unb64).rstrip(b'\0').decode()
        if int(decrypted) == transaction_id:
            return_dict = {'errorD': '', 'error': 200, 'body': ''}
        else:
            return_dict = {'errorD': 'Man in the middle', 'error': 700, 'body': ''}
        return return_dict

    def _action(self, payload, function_):
        payload_json = json.dumps(payload).encode()
        padded_json = payload_json.ljust(32 * (1 + len(payload_json) // 32), b'\0')
        encrypted = self.crypt.encrypt(padded_json)
        payload_json_base64 = base64.b64encode(encrypted)
        data = {'dane': pathname2url(payload_json_base64),
                'appId': self.app_id}
        return _common.request(function_, data)
